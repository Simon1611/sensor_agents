package agents;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.SequentialBehaviour;
import jade.core.behaviours.SimpleBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

public class App extends Agent {
	
	public void setup() {
		addBehaviour(new AppBehaviour());
	}
	
	private class AppBehaviour extends SequentialBehaviour {
		private AID dbAgent;
		
		public void onStart() {
			// adding DF Query for DB Agent
			this.addSubBehaviour(new SimpleBehaviour() {

				@Override
				public void action() {
					// look up the Hub service
					DFAgentDescription template = new DFAgentDescription();
					ServiceDescription sd = new ServiceDescription();
					sd.setType("Database-Service");
					template.addServices(sd);
					
					try {
						DFAgentDescription[] dfds = DFService.search(this.myAgent, template);
						if(dfds.length > 0) {
							dbAgent = dfds[0].getName();
							System.out.println("TS found: " + dbAgent);
						}
					} catch(FIPAException fe) {
						fe.printStackTrace();
					}
				}

				@Override
				public boolean done() {
					boolean ret = false;
					
					if(dbAgent != null) {
						ret = true;
					}
					
					return ret;
				}
				
			});
			
			// add query the hub service
			this.addSubBehaviour(new OneShotBehaviour() {

				@Override
				public void action() {
					ACLMessage m = new ACLMessage(ACLMessage.REQUEST);
					m.addReceiver(dbAgent);
					m.setContent("req(se124532252)");
					this.myAgent.send(m);
				}
				
			});
			
			// add Receive Time behaviour
			this.addSubBehaviour(new SimpleBehaviour() {
				String response = null;
				
				@Override
				public void action() {
					MessageTemplate tmp = MessageTemplate.MatchPerformative(ACLMessage.INFORM);
					ACLMessage m = receive(tmp);
					
					if(m != null) {
						response = m.getContent();
						System.out.println("Response:\n" + response);
					}
				}

				@Override
				public boolean done() {
					boolean done = false;
					if(response != null) {
						done = true;						
					}									
					return done;
				}
				
			});	
		}
		
		public void reset() {
			super.reset();
			dbAgent = null;
		}
	}
	
	
}
