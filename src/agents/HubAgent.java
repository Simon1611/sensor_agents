package agents;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

public class HubAgent extends Agent {
	
	private Connection con;
	
	public void setup() {
		// set name for agent description
		DFAgentDescription dfd = new DFAgentDescription();
		dfd.setName(this.getAID());
		
		// setup register service with DF
		ServiceDescription sd = new ServiceDescription();
		sd.setType("Database-Service");
		sd.setName(this.getLocalName() + "-Database-Service");
		dfd.addServices(sd);
		
		// actually register
		try {
			DFService.register(this, dfd);
		} catch(FIPAException fe) {
			fe.printStackTrace();
		}
		
		// start DB connection
		try {
			// get DB Driver
			Class.forName("com.mysql.jdbc.Driver");
			System.out.println("Connecting to DB...");
			
			// establish connection
			con = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/sensordata?useSSL=false", "root", "mysql");
			
			System.out.println("Connection successfull");			
		} catch(Exception e) {
			
		}
		// start behaviour
		this.addBehaviour(new ServeDatabasePost(this));
	}
	
	public void takeDown() {
		// deregister with df when taken down
		try {
			DFService.deregister(this);
		} catch(FIPAException fe) {
			fe.printStackTrace();
		}
		
		// close DB connection
		try{
			con.close();
			System.out.println("Connection closed");
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	private class ServeDatabasePost extends CyclicBehaviour {
		
		public ServeDatabasePost(Agent a) {
			super(a);
		}
		
		@Override
		public void action() {
			// Build template for expected message
			MessageTemplate tmp = MessageTemplate.MatchPerformative(ACLMessage.REQUEST);
			
			// Try to receive such a message
			ACLMessage message = receive(tmp);
			
			// if there is something
			if(message != null) {
				
				String content = message.getContent();
				
				// if it's a request from an app
				if(content.substring(0, 4).equals("req(")) {
					String sensorID = content.substring(4, content.length()-1);		
					
					ResultSet rs = dataForSensorID(sensorID);
					
					try {
						while(rs.next()) {
							String id = rs.getString(1);
							String date = rs.getString(2);
							int value = rs.getInt(3);
							
							System.out.println("ID: " + id);
							System.out.println("Timestamp: " + date);
							System.out.println("Value: " + value);
							System.out.println("-----------------------------------------");
						}
					} catch(Exception e) {
						e.printStackTrace();
					}
				
				} else { // else it's a post from a sensor so ...
					// read message content and split dataset into datawords
					String[] dataset = message.getContent().split(";", -1); 
					
					// write all words to console
					for(String s : dataset) {
						System.out.println("Dataword: " + s);
					}
					
					// write to database
					insertIntoDb(dataset);
				}
				
				
				
				
				// create a reply
				ACLMessage responseMessage = message.createReply();				
				
				// set message type according to success
				boolean success = true;
				int responseType = success ? ACLMessage.CONFIRM : ACLMessage.NOT_UNDERSTOOD;
				responseMessage.setPerformative(responseType);
				
				// set content
				responseMessage.setContent("");
				
				// send response to sensor
				//this.myAgent.send(responseMessage);
			} else {
				this.block();
			}
		}		
	}
	
	// method for writing sensor data to DB
	public void insertIntoDb(String[] dataset) {
		try {			
			// create statement
			Statement s = con.createStatement();
			
			String q = "INSERT INTO data_int "
						+ "VALUES ('" + dataset[0] + "', '" 
									  + dataset[1] + "', " 
									  + dataset[2] + ");";
			// execute statement
			s.executeUpdate(q);			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	// method for query and retrieve sensor data for a specific ID
	public ResultSet dataForSensorID(String sensorID) {
		ResultSet rs = null;
		
		try {			
			// create statement
			Statement s = con.createStatement();
			
			String q = "SELECT * FROM data_int "
						+ "WHERE sensorID = '" + sensorID + "'";
			// execute statement
			rs = s.executeQuery(q);
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return rs;
	}
}
