package agents;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.DataStore;
import jade.core.behaviours.TickerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.util.leap.Set;

public class SensorAgent extends Agent {

	private AID dbAgent;
	
	public void setup() {
		// set name for agent description
		DFAgentDescription dfd = new DFAgentDescription();
		dfd.setName(this.getAID());
		

		// look up the Hub service on start up
		DFAgentDescription template = new DFAgentDescription();
		ServiceDescription sd = new ServiceDescription();
		sd.setType("Database-Service");
		template.addServices(sd);
		
		try {
			DFAgentDescription[] dfds = DFService.search(this, template);
			if(dfds.length > 0) {
				dbAgent = dfds[0].getName();
				System.out.println("TS found: " + dbAgent);
			}
		} catch(FIPAException fe) {
			fe.printStackTrace();
		}
		
		// add cyclic behaviour to periodically read sensor data
		addBehaviour(new ReadSensorData(this, 1000));
	}
	
	private class ReadSensorData extends TickerBehaviour {
		
		public ReadSensorData(Agent a, long period) {
			super(a, period);
		}
		
		@Override
		public void onTick() {
			// Define Filepath
			String path = "C:\\studium\\sensor\\Sensor_1.txt";
			
			try {
				// init reader
				FileReader fr = new FileReader(path);
				BufferedReader br = new BufferedReader(fr);
				
				// init list
				List<String> linesList = new ArrayList<String>();
				String line = null;
				
				// put file content in list
				while((line = br.readLine()) != null) {
					linesList.add(line);
				}
				
				// reading done
				br.close();
				
				// convert list to array
				String[] lines = new String[linesList.size()];
				linesList.toArray(lines);
				
				// detect value in last line in array
				line = lines[lines.length-1];
				int beginIndex = 0;
				int endIndex = line.length();
				for(int i=0; i<endIndex; i++) {
					if(line.charAt(i) == '=') {
						beginIndex = i;
					}
				}
				
				// read value and convert to int
				int value = Integer.parseInt(line.substring(beginIndex+1));
				
				// get time stamp
				SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss.SSS dd-MM-yy");
				String timestamp = sdf.format(System.currentTimeMillis());
				
				// write to console
				System.out.println("Value: " + value + " at " + timestamp);
				
				// write to Data Store
				this.getDataStore().put(timestamp, value);
				
				// send to Hub
				ACLMessage m = new ACLMessage(ACLMessage.REQUEST);
				m.addReceiver(dbAgent);
				m.setContent(this.myAgent.getLocalName() + ";" + timestamp + ";" + value);
				this.myAgent.send(m);
				
			} catch(IOException e) {
				e.printStackTrace();
			}
		}
		
		@Override
		public int onEnd() {
			DataStore ds = this.getDataStore();
			Set keyset = ds.keySet();
			Object[] keys = keyset.toArray();
			
			System.out.println("Content of Data Store:");
			for(Object key : keys) {
				System.out.println(key.toString() + " --> " + ds.get(key));
			}
			
			return 0;
			
		}
	}
}
